package com.example.apiretrofit

import android.app.Application
import androidx.room.Room
import com.example.apiretrofit.room.CharactersDatabase

class CharacterApplication: Application() {
    companion object{
        lateinit var database: CharactersDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this, CharactersDatabase::class.java,"CharactersDb").build()
    }
}