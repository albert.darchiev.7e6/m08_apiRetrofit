package com.example.apiretrofit.api


class Repository {
    val apiInterface = ApiInterface.create()
    suspend fun getUsers() = apiInterface.getUsers()
    suspend fun getUserById(id:Int) = apiInterface.getUserById(id)
}