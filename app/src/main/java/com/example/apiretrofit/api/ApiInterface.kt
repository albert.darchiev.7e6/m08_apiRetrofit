package com.example.apiretrofit.api

import com.example.apiretrofit.model.CharactersItem
import com.example.apiretrofit.model.CharactersX
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    @GET("/api/v2/Characters")
    suspend fun getUsers(): Response<CharactersX>

    @GET("/api/v2/Characters/{id}")
    suspend fun getUserById(@Path("id") id: Int): Response<CharactersItem>

    companion object {
        val BASE_URL = "https://thronesapi.com"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}