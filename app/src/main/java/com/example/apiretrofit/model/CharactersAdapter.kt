package com.example.apiretrofit.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.apiretrofit.R
import com.example.apiretrofit.databinding.ItemUserBinding
import com.example.apiretrofit.utils.OnClickListener

class CharactersAdapter (private val users: List<CharactersItem>, private val listener: OnClickListener):
    RecyclerView.Adapter<CharactersAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemUserBinding.bind(view)
        fun setListener(got: CharactersItem){
            binding.root.setOnClickListener {
                listener.onClick(got)
            }
        }
    }
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)


    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]
        with(holder){
            setListener(user)
            binding.idTextView.text = user.id.toString()
            binding.fistNameTextView.text = user.fullName
            Glide.with(context)
                .load(user.imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .transform(RoundedCorners(15))
                .into(binding.imageView)
//            println("urlPRINT"+user.imageUrl)
//            println(user.firstName)
        }
    }

}
