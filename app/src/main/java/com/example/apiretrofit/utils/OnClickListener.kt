package com.example.apiretrofit.utils

import com.example.apiretrofit.model.CharactersItem
import com.example.apiretrofit.room.entities.CharactersEntity

interface OnClickListener {
    fun onClick(got: CharactersItem)
}