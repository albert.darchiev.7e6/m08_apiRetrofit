package com.example.apiretrofit.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.apiretrofit.CharacterApplication
import com.example.apiretrofit.api.Repository
import com.example.apiretrofit.model.CharactersItem
import com.example.apiretrofit.model.CharactersX
import com.example.apiretrofit.room.entities.CharactersEntity
import kotlinx.coroutines.*

class QuotesCharactersViewModel: ViewModel() {
    private val repository = Repository()
    var data = MutableLiveData<CharactersX>()
    var isCharacterInFavorites = MutableLiveData<Boolean>()
    var currentCharacter = MutableLiveData<CharactersItem>()

    fun fetchData(){
    CoroutineScope(Dispatchers.IO).launch {
        val response = repository.getUsers()
        withContext(Dispatchers.Main) {
            if(response.isSuccessful){
                data.postValue(response.body())
                            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }
    }

    fun fetchCharacterById(id: Int){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getUserById(id)
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    val character = CharactersX()
                    character.add(response.body() as CharactersItem)
                    data.postValue(character)
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }

    fun fetchFavoriteCharacterById(id: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            data.postValue(CharacterApplication.database.characterDao().getCharacterById(id).map { CharactersEntity -> CharactersItem(CharactersEntity.family, "", CharactersEntity.fullname, CharactersEntity.id, "", CharactersEntity.imageUrl, "", CharactersEntity.title) }.toCollection(CharactersX()))
        }
    }

    fun fetchFavoriteData() {
        CoroutineScope(Dispatchers.IO).launch {
            data.postValue(CharacterApplication.database.characterDao().getAllCharacters().map { CharactersEntity -> CharactersItem(CharactersEntity.family, "", CharactersEntity.fullname, CharactersEntity.id, "", CharactersEntity.imageUrl, "", CharactersEntity.title) }.toCollection(CharactersX()))
        }
    }

    fun characterData(id: Int): CharactersItem?{
        var i = 0
        var item:CharactersItem? = null
        while (i in data.value!!.indices&&item==null){
            if(id == data.value!![i].id) item = data.value!![i]
            i++
        }
        return item
    }

    fun checkIfCharacterIsInFavorites(character: CharactersItem) {
        runBlocking {
            withContext(Dispatchers.IO) {
                val favCharacter = CharacterApplication.database.characterDao().getCharacterById(character.id)
                isCharacterInFavorites.postValue(favCharacter.size != 0)
            }
        }
    }
}
