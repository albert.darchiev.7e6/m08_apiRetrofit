package com.example.apiretrofit.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


//@Parcelize
@Entity(tableName = "CharactersTable")
data class CharactersEntity(
    @PrimaryKey var id: Int,
    var fullname: String,
    var title: String,
    var family: String,
    var favorite: Boolean,
    var imageUrl: String
)//:Parcelable