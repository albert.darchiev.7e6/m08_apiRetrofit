package com.example.apiretrofit.room.dao
import androidx.room.*
import com.example.apiretrofit.room.entities.CharactersEntity

@Dao
interface CharacterDao {
    @Query("SELECT * FROM CharactersTable")
    fun getAllCharacters(): MutableList<CharactersEntity>

    @Query("SELECT * FROM CharactersTable where id = :id")
    fun getCharacterById(id: Int): MutableList<CharactersEntity>

    @Insert
    fun addCharacter(charactersEntity: CharactersEntity)
    @Delete
    fun deleteCharacter(charactersEntity: CharactersEntity)
}
