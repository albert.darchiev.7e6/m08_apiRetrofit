package com.example.apiretrofit.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.apiretrofit.room.dao.CharacterDao
import com.example.apiretrofit.room.entities.CharactersEntity

@Database(entities = [CharactersEntity::class], version = 1)
abstract class CharactersDatabase: RoomDatabase() {
    abstract fun characterDao(): CharacterDao
}