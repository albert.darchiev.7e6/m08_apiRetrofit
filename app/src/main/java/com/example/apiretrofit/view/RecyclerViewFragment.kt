package com.example.apiretrofit.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apiretrofit.databinding.FragmentRecyclerViewBinding
import com.example.apiretrofit.model.CharactersAdapter
import com.example.apiretrofit.model.CharactersItem
import com.example.apiretrofit.room.entities.CharactersEntity
import com.example.apiretrofit.utils.OnClickListener
import com.example.apiretrofit.viewmodel.QuotesCharactersViewModel

lateinit var charactersAdapter: CharactersAdapter
lateinit var linearLayoutManager: RecyclerView.LayoutManager
lateinit var binding: FragmentRecyclerViewBinding
lateinit var quoteViewModel: QuotesCharactersViewModel
var GotId = 0

class RecyclerViewFragment : Fragment(), OnClickListener {
           override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

                binding = FragmentRecyclerViewBinding.inflate(inflater,container,false)
        return binding.root
//        return inflater.inflate(R.layout.fragment_recycler_view, container, false)

            }

    private fun getUsers(): MutableList<CharactersItem>{
        val users = mutableListOf<CharactersItem>()
        println("TESTPRINTL")
        //gotAdapter.notifyDataSetChanged()
        //users.add(GotItem("", "Nom", "fullname",1,"img","https://thronesapi.com/assets/images/daenerys.jpg","cognom","titol"))
        return users
    }
    fun setupRecyclerView(list: List<CharactersItem>){
        charactersAdapter = CharactersAdapter(getUsers(), this)
        val myAdapter = CharactersAdapter(list, this)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimization el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = myAdapter
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        charactersAdapter = CharactersAdapter(getUsers(), this)
        linearLayoutManager = LinearLayoutManager(context)
        setupRecyclerView(getUsers())

        val viewModel = ViewModelProvider(requireActivity())[QuotesCharactersViewModel::class.java]

        viewModel.fetchData()

        viewModel.data.observe(viewLifecycleOwner){
            setupRecyclerView(it)
        }

        binding.searchview.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(queryText: String?): Boolean {
                return false
            }
            // funcionalidad de searchview
            override fun onQueryTextChange(queryText: String?): Boolean {
                if (queryText != "" && queryText!!.length < 7) {
                    // buscar en api por el id
                    viewModel.fetchCharacterById(queryText.toInt())
                }
                else {
                    // volver a enseñar todos los items del api
                    viewModel.fetchData()
                }
                return false
            }
        })
    }

    override fun onClick(got: CharactersItem) {
        val action = RecyclerViewFragmentDirections.actionRecyclerViewFragmentToDetailFragment(got.id)
        val viewModel = ViewModelProvider(requireActivity())[QuotesCharactersViewModel::class.java]
        viewModel.currentCharacter.postValue(got)
        println("TEEEEEE"+ got.id)
        //GotId = got.id
        findNavController().navigate(action)
    }
}