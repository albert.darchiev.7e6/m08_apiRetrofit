package com.example.apiretrofit.view

import com.example.apiretrofit.CharacterApplication
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.apiretrofit.R

import com.example.apiretrofit.databinding.FragmentDetailBinding
import com.example.apiretrofit.room.entities.CharactersEntity
import com.example.apiretrofit.viewmodel.QuotesCharactersViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailFragment : Fragment() {
    lateinit var binding: FragmentDetailBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity())[QuotesCharactersViewModel::class.java]
        val id = arguments?.getInt("id")

        val character = viewModel.currentCharacter.value!!
        viewModel.checkIfCharacterIsInFavorites(character)

        viewModel.isCharacterInFavorites.observe(viewLifecycleOwner) {
            if (it) { binding.saveImageButton.setImageResource(R.drawable.ic_baseline_bookmark_24_v2) }
            else { binding.saveImageButton.setImageResource(R.drawable.ic_baseline_bookmark_24) }
        }

        Glide.with(requireContext())
            .load(character.imageUrl)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.imageView2)
        binding.fullnameTextView.text = character.fullName
        binding.titleTextView.text = character.title
        binding.familyTextView.text = character.family

        binding.saveImageButton.setOnClickListener {
            val char = CharactersEntity(character.id, character.fullName, character.title, character.family, true, character.imageUrl)
             if (viewModel.isCharacterInFavorites.value!!) {
                 deleteCharacter(char)
                 viewModel.isCharacterInFavorites.postValue(false)
             }
             else {
                 insertCharacter(char)
                 viewModel.isCharacterInFavorites.postValue(true)
             }
        }

    }

    private fun insertCharacter(addCharacter: CharactersEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            CharacterApplication.database.characterDao().addCharacter(addCharacter)

        }
    }

    private fun deleteCharacter(addCharacter: CharactersEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            CharacterApplication.database.characterDao().deleteCharacter(addCharacter)

        }
    }
}