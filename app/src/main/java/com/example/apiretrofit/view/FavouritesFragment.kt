package com.example.apiretrofit.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apiretrofit.R
import com.example.apiretrofit.databinding.FragmentFavouritesBinding
import com.example.apiretrofit.model.CharactersAdapter
import com.example.apiretrofit.model.CharactersItem
import com.example.apiretrofit.utils.OnClickListener
import com.example.apiretrofit.viewmodel.QuotesCharactersViewModel

class FavouritesFragment : Fragment(), OnClickListener {

lateinit var binding: FragmentFavouritesBinding
lateinit var linearLayoutManager: RecyclerView.LayoutManager
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavouritesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        charactersAdapter = CharactersAdapter(getUsers(), this)
        linearLayoutManager = LinearLayoutManager(context)
        setupRecyclerView(getUsers())

        val viewModel = ViewModelProvider(requireActivity())[QuotesCharactersViewModel::class.java]
        viewModel.fetchFavoriteData()

        viewModel.data.observe(viewLifecycleOwner){
            setupRecyclerView(it)
        }

        binding.searchview.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(queryText: String?): Boolean {
                return false
            }
            // funcionalidad de searchview
            override fun onQueryTextChange(queryText: String?): Boolean {
                if (queryText != "" && queryText!!.length < 7) {
                    // buscar en api por el id
                    viewModel.fetchFavoriteCharacterById(queryText!!.toInt())
                }
                else {
                    // volver a enseñar todos los items del api
                    viewModel.fetchFavoriteData()
                }
                return false
            }
        })
    }

    fun setupRecyclerView(list: List<CharactersItem>){
        charactersAdapter = CharactersAdapter(getUsers(), this)
        val myAdapter = CharactersAdapter(list, this)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimization el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = myAdapter
        }

    }

    private fun getUsers(): MutableList<CharactersItem>{
        val users = mutableListOf<CharactersItem>()
        println("TESTPRINTL")
        //gotAdapter.notifyDataSetChanged()
        //users.add(GotItem("", "Nom", "fullname",1,"img","https://thronesapi.com/assets/images/daenerys.jpg","cognom","titol"))
        return users
    }

    override fun onClick(got: CharactersItem) {
        val action = FavouritesFragmentDirections.actionFavouritesFragmentToDetailFragment(got.id)
        val viewModel = ViewModelProvider(requireActivity())[QuotesCharactersViewModel::class.java]
        viewModel.currentCharacter.postValue(got)
        println("TEEEEEE"+ got.id)
        //GotId = got.id
        findNavController().navigate(action)
    }
}